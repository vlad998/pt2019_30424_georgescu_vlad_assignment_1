package test;

import junit.framework.TestCase;
import model.Polynom;
import model.PolynomDouble;


/* A class to test the application with JUnitTest. */
public class JUnitTest extends TestCase {

	//take 2 Polynoms
	private Polynom pTest1;
	private Polynom pTest2;
	
	//Test the method printPoly from Polynom Class
	public void testDisplay()
	{
		int arr1[]= {4, 0, 3, 2, 9};
		int arr2[]= {2, 7, 4};
		pTest1= new Polynom(arr1);
		pTest2= new Polynom(arr2);
		
		//we have 2 arrays of coefficients from which 2 polynoms are created and
		//assertEquals test to see if the method printPoly is working fine
		assertEquals("4x^4 + 0x^3 + 3x^2 + 2x^1 + 9", pTest1.printPoly(pTest1));
		assertEquals("2x^2 + 7x^1 + 4", pTest2.printPoly(pTest2));
	}
	
	//Test the method addition from Polynom Class
	public void testAddition()
	{
		int arr1[]= {4, 0, 3, 2, 9};
		int arr2[]= {2, 7, 4};
		pTest1= new Polynom(arr1);
		pTest2= new Polynom(arr2);
		Polynom pTest3= pTest1.addition(pTest2);
		//checks to see if the resulted poly is correct or not
		assertEquals("4x^4 + 0x^3 + 5x^2 + 9x^1 + 13", pTest3.printPoly(pTest3));  
	}
	
	//Test the method subtraction from Polynom Class
	public void testSubtraction()
	{
		int arr1[]= {4, 0, 3, 2, 9};
		int arr2[]= {2, 7, 4};
		pTest1= new Polynom(arr1);
		pTest2= new Polynom(arr2);
		Polynom pTest3= pTest1.subtraction(pTest2);
		//checks to see if the resulted poly is correct or not
		assertEquals("4x^4 + 0x^3 + 1x^2 - 5x^1 + 5", pTest3.printPoly(pTest3));
	}
	
	//Test the method multiplication from Polynom Class
	public void testMultiplication()
	{
		int arr1[]= {4, 0, 3, 2, 9};
		int arr2[]= {2, 7, 4};
		pTest1= new Polynom(arr1);
		pTest2= new Polynom(arr2);
		Polynom pTest3= pTest1.multiplication(pTest2);
		//checks to see if the resulted poly is correct or not
		assertEquals("8x^6 + 28x^5 + 22x^4 + 25x^3 + 44x^2 + 71x^1 + 36", pTest3.printPoly(pTest3));
	}
	
	//Test the method derivative from Polynom Class
	public void testDerivative()
	{
		int arr1[]= {4, 0, 3, 2, 9};
		int arr2[]= {2, 7, 4};
		pTest1= new Polynom(arr1);
		pTest2= new Polynom(arr2);
		Polynom pTestRes1= pTest1.derivative();
		Polynom pTestRes2= pTest2.derivative();
		
		//checks to see if the resulted polynomials are correct or not
		assertEquals("16x^3 + 0x^2 + 6x^1 + 2", pTestRes1.printPoly(pTestRes1));
		assertEquals("4x^1 + 7", pTestRes2.printPoly(pTestRes2));

	}
	
	//Test the method integrate from PolynomDouble Class
	public void testIntegral()
	{
		int arr1[]= {4, 0, 3, 2, 9};
		int arr2[]= {2, 7, 4};
		
		pTest1= new Polynom(arr1);
		pTest2= new Polynom(arr2);
		
		PolynomDouble pTestRes1= PolynomDouble.integrate(pTest1);
		PolynomDouble pTestRes2= PolynomDouble.integrate(pTest2);
		
		//checks to see if the resulted polynomials are correct or not
		assertEquals("0.8x^5 + 0x^4 + 1x^3 + 1x^2 + 9x^1 + 0", pTestRes1.printPoly(pTestRes1));
		assertEquals("0.667x^3 + 3.5x^2 + 4x^1 + 0", pTestRes2.printPoly(pTestRes2));

	}
	
}
