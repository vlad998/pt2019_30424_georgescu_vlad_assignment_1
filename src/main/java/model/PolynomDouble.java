package model;
import java.text.DecimalFormat;
import java.util.*;

/*
 PolynomDouble class describes the integration of a polynomial. the integration operation could not been described into
 Polynom class because the resulted polynomial has double(real numbers) coefficients and the polynomials of type Polynom
 are with integer coefficients only.
 */

public class PolynomDouble{
	public static final int MAX_DEGREE = 25;  //maximum degree
	private double[]coefficient = new double[MAX_DEGREE];  //array of the coefficients
	
	//constructor to set up a polynom with double coefficients
	public PolynomDouble(double coefficient[])
	{
		this.coefficient=coefficient;
	}
	
	/*
	 Method for printing a PolynomDouble. It is exactly the same as the one for printing a Polynom with integer coefficients,
	 the only difference is that this method receives as a parameter a PolynomDouble on which double coefficients we will 
	 operate inside the method.
	 */
	public String printPoly(PolynomDouble p)  
	{
		String s =new String();
		//set precision of the coefficients to 3 decimals after "." after dividing
		DecimalFormat precision = new DecimalFormat("#.###");  
		int exp=p.coefficient.length-1;	
		for(int i=0; i<=p.coefficient.length-1; i++)
		{
			if(p.coefficient[i]<0)
				s+=" - " + precision.format((-1)*p.coefficient[i]) + "x^" + exp;
			else if(i==0)
					s+=precision.format(p.coefficient[i]) + "x^" + exp;
				 else s+= " + " + precision.format(p.coefficient[i]) + "x^" + exp;
					 
			exp--;
		}
		s=s.substring(0, s.length()-3);
		return s;
	}
	
	/* Method for integrating a polyomial. It receives as a parameter a Poolynom with integer coefficients
	 and returns a PolynomDouble resulted after the integration of the polynom received as parameter.
	 */
	public static PolynomDouble integrate(Polynom p1)  
	{
		
		int size=p1.getDegree()+1;  //the resulted polynom has the degree of the integrated polynom +1
		double [] arr = new double[size];   //array of coefficients of the reuslted polynom
		Arrays.fill(arr, 0);  //fill this array with zeroes at the begining
		PolynomDouble p3= new PolynomDouble(arr);  //create the resulted polynom of type PolynomDouble
	//the last coefficient of the resulted polynomial is always 0 because by integrating a constant we get a coefficient* x^1
		p3.coefficient[size-1]=0;  
		for(int i=0; i<p1.getDegree(); i++)  //trace the polynom received as parameter which we want to integrate
			/* at each degree, the resulted poly gets the current coefficient of the integrated poly divided
			by the current degree+1. Since the array of the coefficients gives the degrees in descending order, 
			the last coefficient from the integrated poly will be multiplied with 1 since the integration of a constant is x^1,
			and the last coefficient from the resulted poly will be always 0.
			*/
			p3.coefficient[i]=(double)p1.getCoefficients()[i]/(double)(p1.getDegree()-i);
		
		return p3;
	}

	
}
