package model;
import java.util.*;

/*
 Polynom class which describes the operations on polyomials(addition, subtraction, multiplication, derivative).
 It also contains a method for printing a polynom. Integration operation is being performed into PolynomDouble class
 because the resulted polynomial has double(real numbers) coefficients.
 */

public class Polynom{
		public static final int MAX_DEGREE = 25;     //maximum degree of polynomials
		private int[]coefficient = new int[MAX_DEGREE];   //array of coefficients
		private int degree;   //degree of polynomial
	
		//constructor for setting up the polynomial
		public Polynom(int coefficient[])
		{
			this.coefficient=coefficient;
			degree=coefficient.length;  //assummed the degree to be the length of the array of coefficients
		}
		
		//getter for coefficients
		public int[] getCoefficients() {
		    return this.coefficient;
		}
		
		//getter for degree
		public int getDegree()
		{
			return this.degree;
		}
		
		/* This method prints the polynomial. It receives a Polynom as argument and returns the polynomial
		 as a string, in mathematical form, that is from the highest exponent to the lowest exponent. */
		public String printPoly(Polynom p)  
		{
			String s =new String();
			
			//exp helps for tracing the polynom from the highest exponent to lowest. It has initially the highest exponent of the polynomial  
			int exp=p.coefficient.length-1;	                      
			for(int i=0; i<=p.coefficient.length-1; i++)   //trace the coefficients
			{
				if(p.coefficient[i]<0)   //if the current coefficient is negative
					//then we put in the string a "-" followed by x^ current exponent
					s+=" - " + (-1)*p.coefficient[i] + "x^" + exp;  
				//if the first coefficient is positive, we do not want to display a "+" in front, so we only display 
				//the coefficient followed by x^ current exponent
				else if(i==0)
						s+=p.coefficient[i] + "x^" + exp;
				//otherwise, we simply put the coefficient followed by x^ current exponent
					 else s+= " + " + p.coefficient[i] + "x^" + exp;			 
				exp--;  
			}
			s=s.substring(0, s.length()-3);  //this is for eliminating the x^0 from string. We only want the coefficient in this case
			return s;
		}
	
		
		/* Method for adding two polynomials. It gets as parameter a polynom which will be added with 
		 the one on which the method is called 
		 - If the two added polynomials have the same degree, this is the simple case in which we simply add
		 coefficient by coefficient at each degree until the end 
		 - If poly1 has degree larger than poly2:
		 Since the exponents are in the descending order, we trace the part of the polynomial p1 which has exponents 
		 higher than p2 and simply add the coefficients to resulted poly. Then we trace the part of the polynomials 
		 with common exponents for each poly and add to the resulted poly at the position after the position
		 where we stopped above the coefficient from p1 + the coefficient from p2 at each degree until the end.
		- If poly2 has degree larger than poly1 is the same as above but in this case poly2 has degree larger than poly1	
		 */
		public Polynom addition(Polynom p2)  
		{
			Polynom p1=this;
			int size=Math.max(p1.degree, p2.degree);  //size of the resulting polynom which is the maximum between the added polynomials
			int [] arr = new int[size];   //the array of coefficients of the resulted poly
			Arrays.fill(arr, 0);  //initially, we fill this array with zeroes
			Polynom p3 = new Polynom(arr);  // create a new polynom with the arr. This is the resulted polynom
			
			if(p1.degree==p2.degree){
				for(int i=0; i<size; i++)
					p3.coefficient[i]+=p1.coefficient[i]+p2.coefficient[i];
			}
			
			if(p1.degree>p2.degree){
				int j;
				for(j=0; j<p1.degree-p2.degree; j++)
					p3.coefficient[j]+=p1.coefficient[j];
				for(j=p1.degree-p2.degree; j<p1.degree; j++)  
					p3.coefficient[j]+=p1.coefficient[j]+p2.coefficient[j-p1.degree+p2.degree];
			}
			
			if(p1.degree<p2.degree){
				int j;
				for(j=0; j<p2.degree-p1.degree; j++)
					p3.coefficient[j]+=p2.coefficient[j];
				for(j=p2.degree-p1.degree; j<p2.degree; j++)
					p3.coefficient[j]+=p2.coefficient[j]+p1.coefficient[j-p2.degree+p1.degree];
			}	
			return p3;
		}
	
		
		/* Method for subtracting two polynomials. It gets as parameter a polynom which will be subtracted from 
		 the one on which the method is called.
		 Has the same principle/idea as the method for adding two polynomials but in this case since we are subtracting, 
		 we must take care of signs, that is the only difference. */
		public Polynom subtraction(Polynom p2)  
		{
			Polynom p1=this;
			int size=Math.max(p1.degree, p2.degree);
			int [] arr = new int[size];
			Arrays.fill(arr, 0);
			Polynom p3 = new Polynom(arr);
			
			if(p1.degree==p2.degree){
				for(int i=0; i<size; i++)
					p3.coefficient[i]=p1.coefficient[i]-p2.coefficient[i];
			}
			
			if(p1.degree>p2.degree){
				int j;
				for(j=0; j<p1.degree-p2.degree; j++)
					p3.coefficient[j]=p1.coefficient[j];
				for(j=p1.degree-p2.degree; j<p1.degree; j++)
					p3.coefficient[j]=p1.coefficient[j]-p2.coefficient[j-p1.degree+p2.degree];
			}
			
			if(p1.degree<p2.degree){
				int j;
				for(j=0; j<p2.degree-p1.degree; j++)
					p3.coefficient[j]=-p2.coefficient[j];  //- because we have to take care of signs
				for(j=p2.degree-p1.degree; j<p2.degree; j++)
					p3.coefficient[j]=-p2.coefficient[j]+p1.coefficient[j-p2.degree+p1.degree]; //- because we have to take care of signs
			}	
			return p3;
		}
		
		
		/* Method for multiplying two polynomials. It gets as parameter a polynom which will be multiplied with 
		 the one on which the method is called.
		 */
		public Polynom multiplication(Polynom p2)
		{
			Polynom p1=this;
			int size=p1.degree+p2.degree-1;  //size of the resulted polynomial is the sum of the degrees of the multiplied polys-1
			int [] arr = new int[size]; 
			Arrays.fill(arr, 0);
			Polynom p3 = new Polynom(arr);  // the resulted polynomial
			for(int i=0; i<p1.degree; i++)  // trace the coefficients of the two polys with this two for instructions
				for(int j=0; j<p2.degree; j++)
					//add to the resulted polynomial at index i+j the multiplied coefficients of p1 and p2
					p3.coefficient[i+j] += p1.coefficient[i]*p2.coefficient[j]; 
			return p3;
		}
		
		
		/* Method for derivating a polynomial. It derivates he polynom on which the method is called
		*/
		public Polynom derivative()
		{
			Polynom p1=this;
			int size=p1.degree-1;  //size of the resulted poly. it has the degree of the derivated poly -1
			int [] arr = new int[size];  //array of coefficients of the resulted poly
			Arrays.fill(arr, 0);  //fill this  array with zeroes
			Polynom p3 = new Polynom(arr); //create the resulted poly filled with zeroes at the begining
			if(p1.degree>1) 
			{
			
			for(int i=0; i<p1.degree-1; i++)  //trace the derivated poly
				/* at each degree, the resulted poly gets the current coefficient of the derived poly multiplied
				with the current degree. Since the array of the coefficients gives the degrees in descending order, 
				the last coefficient from the derived poly will disappear since the derivation of a constant is 0.
				*/
					p3.coefficient[i]=p1.coefficient[i]*(p1.degree-1-i);
			}
			//if the degree of the derivated poly is 1(has only 1 oefficient element, the result for derivative is 0)
			if(p1.degree==1) {
				int [] x=new int[1];
				p3= new Polynom(x); 
				p3.coefficient[0]=0;
			}
			return p3;
		}
		
}
