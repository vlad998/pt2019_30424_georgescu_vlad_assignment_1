package view;
import java.awt.*;
import javax.swing.*;


/* The Window class creates the Graphical User Interface */
public class Window extends JFrame {

			//create the GUI elements
    		public JTextField t0=makeTextFields();
    		public JTextField t1=makeTextFields();
    		
    		public JTextArea area=new JTextArea(20, 78);
    		
    		public JButton addition =makeButtons("Addition");
    		public JButton subtraction=makeButtons("Subtraction");
    		public JButton display=makeButtons("Display");
    		public JButton multiplication=makeButtons("Multiplication");
    		public JButton derivative=makeButtons("Derivative");
    		public JButton integrate=makeButtons("Integrate");

		   /* Constructor to setup the GUI
		   The Gui has 2 principal panels, panel, which contains the buttons, labels and the text fields to insert data,
		   and panelOut which contains the text area in which the results will be displayed.
    		*/
		   public Window() {
		        JFrame frame=makeFrame("Polynomial processing");
						        
		        JLabel firstPolyLabel=makeLabels("First Polynomial");
		        JLabel secondPolyLabel=makeLabels("Second Polynomial");
		        
		        JButton b[]= {display, addition, subtraction, multiplication, derivative, integrate}; //an array with the buttons to be inserted into the principal panel
		        
		        JPanel panel=makePrincipalPanels("Poly Panel");
		        JPanel panelOut=makePrincipalPanels("Output Panel");
		        JPanel p1=new JPanel(); //additional panel included in "panel" for helping. not a principal panel
		        
		        frame.add(panel);
		        frame.add(panelOut);
		        
		        panel.add(firstPolyLabel);
		        panel.add(t0); 
		        panel.add(secondPolyLabel);
		        panel.add(t1);
		        
		        includeButtonsInPanel(p1, b);  //adds the buttons in panel p1
		        
		        panelOut.add(area);
		        panel.add(p1);
		        frame.setVisible(true);
		        }
		   
		   
		   //method to create a frame
		   public JFrame makeFrame(String frame_text)
		   {
			  //FRAME
			    JFrame frame = new JFrame(frame_text);
		        frame.setPreferredSize(new Dimension(900, 500));
		        frame.setLayout(new GridLayout(3, 1));
		        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		        frame.pack();
		        frame.setLocationRelativeTo(null);
		        frame.setVisible(true);
		        return frame;
		   }
		 
		   //method to create a button
		   public JButton makeButtons(String button_text)
		   {
			 //BUTTONS
		        JButton button = new JButton(button_text);		        
		        return button;        
		   }
		   
		   //method to create a label
		   public JLabel makeLabels(String label_text)
		   {
			 //LABELS
		        JLabel label = new JLabel();
		        label.setText(label_text); 
		        label.setFont(new java.awt.Font("Times New Roman", 1, 18));
		        return label;
		   }
		   
		   //method to create a text field
		   public JTextField makeTextFields()
		   {
			 //TEXT FIELDS
		        JTextField t= new JTextField();
		        t.setPreferredSize( new Dimension( 250, 30 ));
		        return t;
		   }
		   	  
		   //method to create a principal panel
		   public JPanel makePrincipalPanels(String panel_name)
		   {
			   //PRINCIPAL PANELS panel and panelOut
			   JPanel panel=new JPanel();
			   BoxLayout boxlayout = new BoxLayout(panel, BoxLayout.Y_AXIS);
			   panel.setLayout(boxlayout);
			   panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), panel_name));
		       panel.setBounds(360, 180, 360, 1020);   
			   return panel;
		   }
		   
		   //method to include the buttons into a principal panel
		   public JPanel includeButtonsInPanel(JPanel p, JButton b[])
		   {
			   for(int i=0; i<b.length; i++)
				   p.add(b[i]);
			   return p;
		   }
		   
		   
		   //Method to get the text introduced by the user into the first text field
		   public String getUserInputP1() {
		        return t0.getText();
		    }
		   
		 //Method to get the text introduced by the user into the second text field
		   public String getUserInputP2() {
		        return t1.getText();
		    }
		   
		   //Method to print the result into the text area
		   public void setResult(String s) {
			   area.append(s+"\n");
		    }
		  
		   //Method to show a panel with an error message if the input was wrong inserted
		   public void showError(String errMessage) {
		        JOptionPane.showMessageDialog(this, errMessage);
		    }
		   
	
	}
