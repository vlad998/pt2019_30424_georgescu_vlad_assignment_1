package control;

import java.awt.event.*;

import model.*;
import view.*;

/* Control class links the graphical user interface to the polynom methods. It extends Window class and inherits all
 its attributes and methods and also implements listeners on buttons. */
public class Control extends Window implements ActionListener {

	public Control() {
		display.addActionListener(this);
		addition.addActionListener(this);
		subtraction.addActionListener(this);
		multiplication.addActionListener(this);
		derivative.addActionListener(this);
		integrate.addActionListener(this);
	}

	// Method to read the coefficients of the polynomials. The polynomials are read
	// as in a mathematical assistance software,
	// such as Matlab. The coefficients gives also the exponents of the polynomials
	// in descending order.
	// E.g.: If we insert the following coefficients: 2 9 7, we have the polynomial
	// 2x^2 + 9x^1 + 7
	public int[] readCoefficients(String s) {
		String line = s;
		// break the string given as an input into small strings, one string for each
		// coefficient
		String[] nrCoef = line.split(" ");
		nrCoef = line.split(" "); // split the input into a new small string when space was found in the initial
									// string
		int[] coefficients = new int[nrCoef.length];
		for (int i = 0; i <= nrCoef.length - 1; i++) {
			// exception to handle wrong data inserted by the user. Only integers separated
			// by a space are allowed
			try {
				coefficients[i] = Integer.parseInt(nrCoef[i]);
			} catch (NumberFormatException e) {
				showError(
						"Invalid Input. Please insert only the coefficients of the polynomials separated by space!" +  '\n' 
				+ "The coefficients give the exponents of the polynomials." + "\n" +
				"The polynomials are of form anX^n-1 + an-1X^n-2 + ... +a2x^1 + a1 ");
				throw new IllegalArgumentException("Not a number: " + nrCoef + " at index " + i, e);
			}
		}
		return coefficients;
	}

	// method to add the operations to the corresponding buttons
	public void actionPerformed(ActionEvent e) {
		String userInputP1 = getUserInputP1();
		String userInputP2 = getUserInputP2();
		Polynom p1 = new Polynom(readCoefficients(userInputP1));
		Polynom p2 = new Polynom(readCoefficients(userInputP2));
		if (e.getSource() == display) {
			setResult("Poly1 = " + p1.printPoly(p1) + "\n" + "Poly2 = " + p2.printPoly(p2));
		}
		if (e.getSource() == addition) {
			Polynom pResulted = p1.addition(p2);
			setResult("Poly1 + Poly2 = " + pResulted.printPoly(pResulted));
		}
		if (e.getSource() == subtraction) {
			Polynom pResulted = p1.subtraction(p2);
			setResult("Poly1 - Poly2 = " + pResulted.printPoly(pResulted));
		}
		if (e.getSource() == multiplication) {
			Polynom pResulted = p1.multiplication(p2);
			setResult("Poly1 * Poly2 = " + pResulted.printPoly(pResulted));
		}
		if (e.getSource() == derivative) {
			Polynom p1Resulted = p1.derivative();
			Polynom p2Resulted = p2.derivative();
			setResult("Poly1 derivated = " + p1Resulted.printPoly(p1Resulted) + "\n" + "Poly2 derivated = " + p2Resulted.printPoly(p2Resulted));
		}
		if (e.getSource() == integrate) {
			PolynomDouble p1Resulted = PolynomDouble.integrate(p1);
			PolynomDouble p2Resulted = PolynomDouble.integrate(p2);
			setResult("Poly1 integrated = " + p1Resulted.printPoly(p1Resulted) + "\n" + "Poly2 integrated = " + p2Resulted.printPoly(p2Resulted));
		}
	}
	
}