package application;

import control.Control;


/* The program's driver class to run the application */
public class Main{
	
	public static void main(String[] args)
    {
       new Control();
    }

}
